This project serves as a template for FreeRTOS applications using meson for the
STM32L052K8. It copies relevant FreeRTOS header and source files into include
and src, respectively. The Meson build system assumes the CMSIS system
definitions are provided by a libcmsis5 library, and that the start/crt0 is
provided by a libstart library. The way this is configured is to leverage the
work in git@gitlab.com:gemarcano/stm32_cmsis_headers.git .
